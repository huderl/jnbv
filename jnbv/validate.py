#!/usr/bin/env python

#####################
# IMPORT LIBRARIES ##
#####################

# General systems
import os
import sys
import argparse
import json
import datetime

# Handling ipynb notebooks
import nbformat
import difflib

# Papermill
import papermill as pm

# Logging results
import logging

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
YELLOW = "\033[0;33m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


#############
# FUNCTIONS #
#############

def setup_logger(logfile_name, remove_if_exists, debug):
    """ Setup up a simple log file"""

    new_file = False

    # Check if this log file already exists
    if os.path.exists(logfile_name):
        new_file = False
        if debug:
            print('Log file exists:', logfile_name)
    else:
        new_file = True
        if debug:
            print('Log file does not exist:', logfile_name)

    # Delete the file first if desired, and it already exists
    if remove_if_exists and os.path.exists(logfile_name):
        os.remove(logfile_name)
        new_file = True
        if debug:
            print('Log file deleted:', logfile_name)

    if new_file:

        # If making a new log file, remove the old logging handler, if it
        # exists
        log = logging.getLogger()
        for hdlr in log.handlers[:]:  # remove the existing file handlers
            if isinstance(hdlr, logging.FileHandler):
                log.removeHandler(hdlr)

    # Set logging filename, format, logging level
    FORMAT = "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s\n" \
        "  %(message)s"
    logging.basicConfig(filename=logfile_name, level=logging.INFO,
                        format=FORMAT)

    # Log some information
    if new_file:
        logging.info('Created log file: ' + str(logfile_name))
        if debug:
            print('Created log file:', logfile_name)
    else:
        logging.info('Appending to log file: ' + str(logfile_name))
        if debug:
            print('Appending to log file:', logfile_name)


def create_output_dir(kernel_name, debug):

    # Label outputs with current datetime
    dt_date = datetime.datetime.now()
    dt_date_str = dt_date.strftime("%Y-%m-%d_%H-%M-%S")
    if debug:
        print("Current datetime:    ", dt_date)

    # Create the output directory for the kernel
    kernel_dir = "test-results/" + kernel_name
    if not os.path.isdir(kernel_dir):
        os.makedirs(kernel_dir, exist_ok=True)
        os.chmod(kernel_dir, mode=0o777)

    # Create the output directory for this particular set of tests
    output_dir = kernel_dir + "/" + dt_date_str + "/"
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir, exist_ok=True)
        os.chmod(output_dir, mode=0o777)

    if debug:
        print("output_dir:          ", output_dir)

    return output_dir


def create_output_files(notebook_filename, output_dir, debug):

    basename = os.path.basename(notebook_filename)
    output_ipynb = output_dir + basename
    output_log = (output_dir + os.path.splitext(basename)[0] +
                  ".log")
    if os.path.isdir(output_dir):
        touch(output_ipynb)
        os.chmod(output_ipynb, mode=0o666)
        touch(output_log)
        os.chmod(output_log, mode=0o666)

    if debug:
        print("notebook_filename:   ", notebook_filename)
        print("output_ipynb:        ", output_ipynb)
        print("output_log:          ", output_log)

    return output_ipynb, output_log


def touch(fname):
    try:
        os.utime(fname, None)
    except OSError:
        open(fname, 'a').close()


def execute_ipynb_pm(input_name, output_name, kernel_name, debug):

    print("")
    print(LIGHTPURPLE, '*'.center(80, '*'), NC)
    print(LIGHTPURPLE, "Executing notebook ", NC, input_name)
    print(BLUE, "Using kernel       ", NC, kernel_name)

    logging.info('Executing notebook: ' + str(input_name))

    successful_execution = True
    try:
        # For options see:
        #   https://github.com/nteract/papermill/blob/main/papermill/execute.py
        pm.execute_notebook(
            input_name,
            output_name,
            kernel_name=kernel_name,
            progress_bar=False,
            log_output=True,
            report_mode=False
        )
    except ModuleNotFoundError as e:
        print(RED)
        print("ModuleNotFoundError")
        print(e)
        print(NC)
        logging.error("ModuleNotFoundError")
        logging.error(e)
        successful_execution = False
    except BaseException as e:
        print(RED)
        print("Unknown error")
        print(e)
        print(NC)
        logging.error("Unknown error")
        logging.error(e)
        successful_execution = False

    logging.info('Done executing notebook: ' + str(input_name))
    logging.info('Output saved to: ' + str(output_name))

    print(LIGHTPURPLE, "Done executing")
    print(LIGHTPURPLE, "Output Saved to    ", NC, output_name)
    print(LIGHTPURPLE, '*'.center(80, '*'), NC)
    print("")

    return successful_execution


def read_ipynb(path, quiet, debug):

    if not quiet:
        print("")
        print(LIGHTPURPLE, '*'.center(80, '*'), NC)
        print(LIGHTPURPLE, "Reading output notebook", NC, path)
        print("")

        logging.info('Reading ' + str(path))

    if debug:
        print(path)

    nb = []
    errors = []

    # Check if the file exists and is accessible
    if not os.access(path, os.R_OK):
        if not quiet:
            print(RED, "FILE NOT ACCESSIBLE", NC)
        errors.append("FILE NOT ACCESSIBLE")

    else:
        # Read the notebook
        nb = nbformat.read(
            path,
            nbformat.current_nbformat
        )

        if debug:
            print("")

        # Loop over cells, look for errors
        for cell in nb.cells:
            if debug:
                print(cell)

            # Skip some types of cells
            if "cell_type" in cell:
                if cell["cell_type"] == 'markdown':
                    if debug:
                        print(YELLOW, "skipping cell_type:", cell["cell_type"],
                              NC)
                    continue

            if not quiet:
                if "source" in cell and "cell_type" in cell:
                    if cell["cell_type"] == "code" and cell["source"] != "":
                        logging.info("cell source: " + str(cell["source"]))
                        print(BLUE, '-'.center(80, '-'), NC)
                        print(BLUE, "source: ", NC, cell["source"])

            if "outputs" in cell:
                COLOR = CYAN
                if len(cell["outputs"]) == 0:
                    if not quiet:
                        print(COLOR, "cell output: ", str(cell["outputs"]), NC)
                        logging.info("cell output: " + str(cell["outputs"]))
                        print(BLUE, '-'.center(80, '-'), NC)
                        print("")
                else:
                    for output in cell["outputs"]:
                        # Search for errors in the output
                        if output.output_type == "error":
                            errors.append(output)
                            if not quiet:
                                COLOR = RED
                                logging.error("cell output: " + str(output))
                        else:
                            if not quiet:
                                logging.info("cell output: " + str(output))

                        if not quiet:
                            print(COLOR, "cell output: ", NC)
                            print_cropped_output(output)

                    if not quiet:
                        if len(cell["outputs"]) > 0:
                            print(BLUE, '-'.center(80, '-'), NC)
                            print("")

    if not quiet:
        logging.info('Done reading ' + str(path))

        print(LIGHTPURPLE, "Done reading")
        print(LIGHTPURPLE, '*'.center(80, '*'), NC)
        print("")

    if debug:
        if nb != []:
            nb_dict = from_nbnode(nb)
            print(nb_dict)

    return nb, errors


def from_nbnode(value):
    """Recursively convert NotebookNode to dict."""
    if isinstance(value, nbformat.NotebookNode):
        return {k: from_nbnode(v) for k, v in value.items()}
    return value


def test_ipynb(errors):
    assert errors == []


def test(notebook_filename, debug):

    # First read the ipynb notebook into a python dictionary
    nb, errors = read_ipynb(notebook_filename, False, debug)

    print("")
    print(LIGHTPURPLE, '*'.center(80, '*'), NC)
    print(LIGHTPURPLE, "Testing output notebook:\n", NC, notebook_filename)

    # Check for errors
    logging.info('Testing output notebook' + str(notebook_filename))

    test_result = False
    try:
        test_result = test_ipynb(errors)
        test_result = True
    except AssertionError as e:
        print("")
        print(RED, "AssertionError", NC)
        print(e)
        logging.error("AssertionError")
        logging.error(e)
    except BaseException as e:
        print("")
        print(RED, "Unknown error", NC)
        print(e)
        logging.error("Unknown error")
        logging.error(e)

    # Print final result
    COLOR = GREEN
    if not test_result:
        COLOR = RED
    print(COLOR, '-'.center(80, '-'))
    print(COLOR, " test result: " + str(test_result))
    print(COLOR, '-'.center(80, '-'), NC)

    # Log results
    logging.info("test result: " + str(test_result))
    logging.info('Done testing ' + str(notebook_filename))

    print(LIGHTPURPLE, "Done testing")
    print(LIGHTPURPLE, '*'.center(80, '*'), NC)
    print("")

    return test_result


def compare_ipynb_notebooks(notebook_filenames, debug):
    """Compare sources and outputs in two notebooks.
       Return True or False and any errors found."""

    # Print and log what is going to happen
    print("")
    print(LIGHTPURPLE, '*'.center(80, '*'), NC)
    print(LIGHTPURPLE, 'Comparing outputs of:', NC)
    print('  ' + str(notebook_filenames[0]) + '\n'
          '  ' + str(notebook_filenames[1]))
    print("")
    logging.info('Comparing outputs of ' + str(notebook_filenames[0]) +
                 '\n' + str(notebook_filenames[1]))

    # First read the ipynb notebooks into python dictionaries
    notebook_output = []
    notebook_errors = []
    for notebook_filename in notebook_filenames:
        nb, errors = read_ipynb(notebook_filename, True, debug)
        notebook_output.append(nb)
        notebook_errors.append(errors)

    # Check for empty notebooks
    comparison_errors = []
    continue_comparison = True
    if "cells" not in notebook_output[0] or "cells" not in notebook_output[1]:
        message = "EMPTY NOTEBOOK!"
        comparison_errors.append(message)
        logging.error(message)
        continue_comparison = False

    # Check if numbers of cells in the notebooks is different
    if continue_comparison:
        if len(notebook_output[0].cells) != len(notebook_output[1].cells):
            error_message = "number of cells is different in the two notebooks"
            print(RED, error_message)
            comparison_errors.append(error_message)
            logging.error(error_message)
            continue_comparison = False

    # Continue for notebooks that are not empty and have the same numbers of
    # cells
    if continue_comparison:

        # Loop over the cells in the notebooks
        for cells in zip(notebook_output[0].cells, notebook_output[1].cells):

            # Comparisons only made for source cells that contain code and are
            # not empty
            continue_comparison = should_cells_be_compared(cells[0], cells[1],
                                                           debug)
            if continue_comparison:

                # Check if source cells are the same or if they contain
                # special keywords
                ignore_output_differences, source_errors = \
                    compare_source_lines(
                        cells[0]["source"], cells[1]["source"], debug)
                comparison_errors.extend(source_errors)

                len_0_output = len(cells[0]["outputs"])
                len_1_output = len(cells[1]["outputs"])

                # Empty output is not necessarily bad, print it
                if len_0_output == 0 or len_1_output == 0:
                    print(BLUE, '-'.center(80, '-'), NC)
                    if len_0_output == 0:
                        print(CYAN, "cells[0][outputs]: ", NC)
                        print_cropped_output(cells[0]["outputs"])
                    if len_1_output == 0:
                        print(CYAN, "cells[1][outputs]: ", NC)
                        print_cropped_output(cells[1]["outputs"])
                    print(BLUE, '-'.center(80, '-'), NC)

                # # NOTE: Commented out this seciton 2021-05-26, need to
                #         think more about it...
                # # Check if size of the output arrays are different
                # if len_0_output != len_1_output:
                #     message = "output sizes not the same"
                #     print(RED, message, NC)
                #     print(len_0_output)
                #     for output in cells[0]["outputs"]:
                #         print("  ", output)
                #     print(len_1_output)
                #     for output in cells[1]["outputs"]:
                #         print("  ", output)
                #     logging.error(message)
                #     comparison_errors.append(cells[0]["outputs"])
                #     comparison_errors.append(cells[1]["outputs"])

                # Loop over outputs - can have multiple for each source cell
                for outputs in zip(cells[0]["outputs"], cells[1]["outputs"]):

                    print(BLUE, '-'.center(80, '-'), NC)

                    # Print the output lines
                    for idx, output in enumerate(outputs):
                        logging.info("outputs[" + str(idx) + "] : "
                                     + str(output))
                        print(CYAN, "outputs[", str(idx), "]: ", NC)
                        # Crop long output to terminal
                        print_cropped_output(output)

                    print(BLUE, '-'.center(80, '-'), NC)

                    if outputs[0] == outputs[1]:
                        message = "outputs are the same"
                        print(GREEN)
                    else:
                        # Some differences can be ignored - look closer for
                        # those
                        comparison_pass = examine_output_diff(
                            outputs[0], outputs[1], debug)

                        if ignore_output_differences:
                            comparison_pass = True
                        if comparison_pass:
                            print(GREEN)
                            message = "outputs are almost the same"
                        else:
                            print(RED)
                            message = "outputs are not the same"
                            comparison_errors.append(outputs[0])
                            comparison_errors.append(outputs[1])

                    print(message)
                    logging.info(message)
                    print(NC)

    # Check comparisons for errors (differences)
    comparison_result = False
    error_message = ""
    error_content = ""
    try:
        comparison_result = test_ipynb(comparison_errors)
        comparison_result = True
    except AssertionError as e:
        error_message = "AssertionError"
        error_content = e
    except BaseException as e:
        error_message = "Unknown Error"
        error_content = e

    return comparison_result, error_message, error_content


def print_comparison_results(notebook_filenames, comparison_result,
                             error_message, error_content, debug):

    # Print & log errors
    if error_message != "":
        print("\n", RED, error_message, "\n", error_content)
        logging.error(error_message)
        logging.error(error_content)

    if comparison_result:
        print(GREEN)
    else:
        print(RED)

    print('-'.center(80, '-'))
    print(" comparison result: " + str(comparison_result))
    print('-'.center(80, '-'), NC)

    # Log results
    logging.info("comparison result: " + str(comparison_result))
    logging.info('Done Comparing outputs of ' + str(notebook_filenames[0])
                 + '\n' + str(notebook_filenames[1]))

    # Print results
    print("")
    print(LIGHTPURPLE, 'Done Comparing outputs of:', NC)
    print('  ' + str(notebook_filenames[0]) + '\n'
          '  ' + str(notebook_filenames[1]))
    print(LIGHTPURPLE, '*'.center(80, '*'), NC)
    print("")

    return comparison_result


def should_cells_be_compared(cells_0, cells_1, debug):
    """Check to see if source cell is code, is not empty"""

    if debug:
        print("cells_0:", cells_0)
        print("cells_1:", cells_1)

    continue_comparison = True
    if "cell_type" not in cells_0 or "cell_type" not in cells_1:
        continue_comparison = False

    if continue_comparison:
        # Only continue for code, skip comments/markdown
        if cells_0["cell_type"] != "code" or \
                cells_1["cell_type"] != "code":
            continue_comparison = False

    if continue_comparison:
        # Check that both source and output cells exist in the
        # notebooks
        if "source" not in cells_0 or \
                "outputs" not in cells_0 or \
                "source" not in cells_1 or \
                "outputs" not in cells_0:
            continue_comparison = False

    if continue_comparison:
        # Check for empty cells
        if cells_0["source"] == "" or \
                cells_1["source"] == "":
            continue_comparison = False

    return continue_comparison


def compare_source_lines(source_line_0, source_line_1, debug):

    # Some output for given sources can be ignored, e.g. output from timing
    # finctions will not be exactly the same
    ignore_output_if_source = ['timeit', 'random_directions']
    ignore_output_differences = False

    # Look at the source lines
    logging.info("source_line_0: " + str(source_line_0))
    logging.info("source_line_1: " + str(source_line_1))

    print(BLUE, '-'.center(80, '-'), NC)
    print(BLUE, "source_line_0: ", NC, source_line_0)
    print(BLUE, "source_line_1: ", NC, source_line_1)
    print(BLUE, '-'.center(80, '-'), NC)

    # Compare the two source lines
    message = ""
    comparison_errors = []
    if source_line_0 == source_line_1:
        message = "sources are the same"
        print(GREEN)

        # Ignore output from certain sources
        for source_item in ignore_output_if_source:
            if source_item in source_line_0:
                ignore_output_differences = True
    else:
        message = "sources are not the same"
        print(RED)
        comparison_errors.append(source_line_0)
        comparison_errors.append(source_line_1)
        logging.error(message)
    print(message, NC)
    print("")

    return ignore_output_differences, comparison_errors


def print_cropped_output(output):
    """Crop long output to the terminal"""

    if len(str(output)) > 1000:
        print(str(output)[:500],
              YELLOW, "\n   <output cropped> \n", NC,
              str(output)[-500:])
    else:
        print(output)


def examine_output_diff(nb0, nb1, debug):
    """Look closer at differences between output from two notebooks"""

    if debug:
        print(GREEN, "** Entering examine_output_diff **", NC)
        print(CYAN, type(nb0), NC)
        print(CYAN, type(nb1), NC)

    comparison_pass = True

    # Some keys, subkeys, and values to be ignored
    keys_to_look_for = ['data', 'execution_count', 'text']
    keys_to_ignore = ['execution_count']

    # Loop over all keys in the first dictionary
    for key in nb0:

        if debug:
            print(BLUE, "  key:", NC, key)

        # Check if the values are the same for the same key in the two
        # dictionaries
        if key not in nb1:
            comparison_pass = False
            if debug:
                print(RED, 'comparison_pass: ', comparison_pass, NC)
                print(RED, "    key ", key, "not in nb1", NC)

        else:
            # The easy case - values are exactly the same, nothing more to do
            if nb0[key] == nb1[key]:
                if debug:
                    print(GREEN, "    same key values", NC)

            else:
                if debug:
                    print(RED, "    different key values", NC)

                # If the values are different, take a closer look - maybe this
                # difference can be ignored
                if key in keys_to_look_for:

                    # For certain keys, completely ignore differences in the
                    # values
                    if key in keys_to_ignore:
                        if debug:
                            print(GREEN, "ignore ", key, NC)

                    # For other keys, ignore certain differences in the values
                    if key == 'data':
                        key_data_comparison_pass = examine_output_key_data(
                            key, nb0, nb1, debug)
                        if not key_data_comparison_pass:
                            comparison_pass = False

                    if key == 'text':
                        key_text_comparison_pass = examine_output_key_text(
                                key, nb0, nb1, debug)
                        if not key_text_comparison_pass:
                            comparison_pass = False

                else:
                    comparison_pass = False
                    if debug:
                        print(RED, 'comparison_pass: ', comparison_pass, NC)
                        print(RED, "    key ", key, "not in keys_to_look_for",
                              NC)

    if debug:
        print('comparison_pass: ', comparison_pass)
        print(GREEN, "** Leaving examine_output_diff **", NC)

    return comparison_pass


def examine_output_key_data(key, nb0, nb1, debug):

    comparison_pass = True

    subkey_values_to_ignore = {}
    subkey_values_to_ignore['data'] = {
        'text/html': ['h5glance', 'image/png'],
        'application/vnd.jupyter.widget-view+json': ['model_id'],
        'text/plain': ['h5glance', 'matplotlib.']
    }

    for subkey in nb0[key]:
        if debug:
            print(BLUE, "    --> subkey:", NC, subkey)

        if nb0[key][subkey] == nb1[key][subkey]:
            if debug:
                print(GREEN, "      same subkey values",
                      NC)
        else:
            if debug:
                print(RED, "      different subkey values",
                      NC)

            # For images, sometimes they were not exactly
            # the same, though could not tell by eye -
            # should investigate further perhaps?
            if subkey == 'image/png':
                image_comparison = compare_images(
                    nb0[key][subkey], nb1[key][subkey],
                    debug)
                if not image_comparison:
                    comparison_pass = False

            else:
                # For certain sub-keys, ignore differences
                # in the values
                if subkey in subkey_values_to_ignore[key]:
                    found_subkey_to_ignore = False
                    for subkey_value in \
                            subkey_values_to_ignore[
                                key][subkey]:
                        if subkey_value in \
                                nb0[key][subkey]:
                            found_subkey_to_ignore = True
                            if debug:
                                print(GREEN,
                                      "       ignoring...",
                                      subkey_value, NC)
                    comparison_pass = \
                        found_subkey_to_ignore
                else:
                    comparison_pass = False
                    if debug:
                        print(RED, 'comparison_pass: ',
                              comparison_pass, NC)
                        print(RED, "    subkey not in ",
                              "subkey_values_to_ignore",
                              "[key]", NC)

    return comparison_pass


def examine_output_key_text(key, nb0, nb1, debug):

    if debug:
        print("Entered examine_output_key_text()")

    comparison_pass = True

    key_values_to_ignore_AND = {}
    key_values_to_ignore_AND['text'] = [
        'Test: dtype:',
        'Elapsed Time',
        'Reconstructing 1 slice groups',
        'liquid fraction',
        'R2019a',
        'Mpix',
        'Gflops',
        'naive GPU',
        'HDF5_PLUGIN_PATH',
        'VisibleDeprecationWarning',
        'MatplotlibDeprecationWarning',
        'Hello',
        'hello'
    ]

    key_values_to_ignore_OR = {}
    key_values_to_ignore_OR['text'] = [
        'R2019a',
        'HDF5_PLUGIN_PATH',
        'Hello',
        'hello'
    ]

    if debug:
        print(NC, "      key: ", key, NC)
        print(NC, "      values: ", NC)
        print(NC, "         nb0[", key, "]: ", nb0[key],
              NC)
        print(NC, "         nb1[", key, "]: ", nb1[key],
              NC)

    found_key_to_ignore = False
    if key in key_values_to_ignore_AND:
        for value in key_values_to_ignore_AND[key]:
            if value in nb0[key] and value in nb1[key]:
                found_key_to_ignore = True
                if debug:
                    print(GREEN, "      ignore", key,
                          "differences", NC)
        comparison_pass = found_key_to_ignore

    if key in key_values_to_ignore_OR:
        for value in key_values_to_ignore_OR[key]:
            if value in nb0[key] or value in nb1[key]:
                found_key_to_ignore = True
                if debug:
                    print(GREEN, "      ignore", key,
                          "differences", NC)
        comparison_pass = found_key_to_ignore

    # Check for the occaisional \r
    if '\r' in nb0[key] or '\r' in nb1[key]:
        if nb0[key].replace('\r', '') == \
                nb1[key].replace('\r', ''):
            comparison_pass = True
            if debug:
                print(GREEN, "      ignore", key,
                      "differences", NC)

    return comparison_pass


def compare_images(image_0, image_1, debug):
    """Sometimes images may look visually the same, but the png value if
       printed out is not the same.  Look to see if two images are
       close enough."""

    png_diff = difflib.SequenceMatcher(None, a=image_0, b=image_1).ratio()

    # This png difference doesn't seem too
    # reliable....
    image_comparison = True
    # if png_diff > 0.9:
    if png_diff > 0.00001:
        if debug:
            print(GREEN, "        ignoring...",
                  "png_diff: ", png_diff, NC)
    else:
        image_comparison = False
        if debug:
            print(RED, 'image_comparison: ',
                  image_comparison, NC)
            print(RED, "    png_diff <= 0.9",
                  ", png_diff: ", png_diff, NC)
    return image_comparison


def parse_input_names(input_notebook_filenames, debug):
    """Parse the input filenames string or list, return list"""

    # Input could be a long string of names, or a list
    notebook_filenames = []
    for input_notebook_filename in input_notebook_filenames:
        notebook_filenames.extend(input_notebook_filename.split())
    if debug:
        print("")
        print("Notebook Filenames:")
        print(json.dumps(notebook_filenames, indent=4))

    if notebook_filenames == []:
        print(RED, "At least one argument (one or more ipynb filenames)",
              "required")
        print("")
        assert False

    # Check if files exist, assert error if not
    files_exist = True
    for notebook_filename in notebook_filenames:
        if os.path.isfile(notebook_filename):
            if debug:
                print(GREEN, notebook_filename, "exists", NC)
        else:
            print(RED, notebook_filename, "does not exist", NC)
            files_exist = False

    if not files_exist:
        print(RED, "One more more files do not exist")
        print("")
        assert False

    return notebook_filenames


def dummy_test():
    return True


########
# MAIN #
########

def main(argv):
    """The main function - usage and help, argument parsing"""

    # Setup options
    parser = argparse.ArgumentParser(
        description='Execute Jupyter notebooks (.ipynb format) in the '
                    'terminal. Compare outputs from different executions,'
                    ' save results.')
    parser.add_argument("input_notebook_filenames", nargs='*',
                        help='The input Jupyter notebook file names')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='debug output')
    parser.add_argument('--execute', action='store_true',
                        help='Execute the notebook, optionally save to new '
                             'file with --output_ipynb option.')
    parser.add_argument('--read', action='store_true',
                        help='Read the given Jupyter notebook file.')
    parser.add_argument('--test', action='store_true',
                        help='Check the given Jupyter notebook (.ipynb) file'
                             ' for errors')
    parser.add_argument('--compare', action='store_true',
                        help='Compare output of notebook execution with'
                             ' original execution')
    parser.add_argument('--compare_with', required=False, default=False,
                        help='Comparison Jupyter notebook filename. ')
    parser.add_argument('--kernel_name', required=False, default=False,
                        help='The name of the kernel to use when executing '
                             ' a notebook.')
    parser.add_argument('--save', action='store_true',
                        help='Save output to test_results/')
    parser.add_argument('--output_ipynb', required=False,
                        default="output.ipynb",
                        help='Output Jupyter notebook filename. The default '
                             'is a temporary file that is not saved.')
    parser.add_argument('--log_file', required=False, default=False,
                        help='Output results of action to a log file. The '
                             'default is no log file.')

    # Print a little extra in addition to the standard help message
    if len(argv) == 0 or '-h' in argv or '--help' in argv:
        try:
            args = parser.parse_args(['-h'])
        except SystemExit:
            print('')
            print('Examples of usage:')
            print('  python3 validate.py'
                  ' --execute my_notebook.ipynb')
            sys.exit()
    else:
        args = parser.parse_args(argv)

    if args.debug:
        print("")
        print("Input arguments:")
        print(json.dumps(vars(args), indent=4))
        print("")

    # Parse input file names, check if they exist
    notebook_filenames = parse_input_names(args.input_notebook_filenames,
                                           args.debug)

    # Create output directory
    if args.save:
        output_dir = create_output_dir(args.kernel_name, args.debug)

    kernel_notebook_test_results = {}
    kernel_test_results = True
    complete_test_result = True

    # Loop over input notebook files
    for notebook_filename in notebook_filenames:

        # Create output log and ipynb notebook, using default locations
        output_ipynb = ""
        if args.save:
            output_ipynb, output_log = create_output_files(
                notebook_filename, output_dir, args.debug)
            setup_logger(output_log, False, args.debug)
        else:
            output_ipynb = "output.ipynb"

        # Execute an ipynb notebook, save output to new ipynb notebook
        if args.execute:

            # Print somthing extra if test and comparison will also be done
            if args.test and args.compare:
                print("")
                print(CYAN, '*'.center(80, '*'), NC)
                print(CYAN, "** STARTING VALIDATIONS FOR:")
                print(CYAN, "**", NC, "  Kernel:   ", args.kernel_name)
                print(CYAN, "**", NC, "  Notebook: ", notebook_filename)
                print(CYAN, "** OUTPUT:")
                print(CYAN, "**", NC, "  Notebook:", output_ipynb)
                if args.save:
                    print(CYAN, "**", NC, "  Log:     ", output_log)
                print(CYAN, '*'.center(80, '*'), NC)

            # Execute the notebook with papermill
            successful_execution = execute_ipynb_pm(
                notebook_filename, output_ipynb, args.kernel_name,
                args.debug)

            # Test for errors
            if args.test:
                test_result = test(output_ipynb, args.debug)

            # Compare with original
            if args.compare:
                comparison_result, error_message, error_content = \
                    compare_ipynb_notebooks(
                        [notebook_filename, output_ipynb], args.debug)
                print_comparison_results([notebook_filename, output_ipynb],
                                         comparison_result, error_message,
                                         error_content, args.debug)

            # If also comparing and testing, print results for this notebook
            if args.test and args.compare:
                result_color = RED
                result_word = "FAILED"
                if successful_execution and test_result and comparison_result:
                    result_color = GREEN
                    result_word = "PASSED"

                print(CYAN, '*'.center(80, '*'), NC)
                print(CYAN, "** FINISHED VALIDATIONS FOR:")
                print(CYAN, "**", NC, "  Kernel:   ", args.kernel_name)
                print(CYAN, "**", NC, "  Notebook: ", notebook_filename)
                print(CYAN, "** RESULT:", result_color, result_word)
                print(CYAN, '*'.center(80, '*'), NC)
                print("")

                # Log the result
                if args.save:
                    logging.info('FINAL RESULT: ' + str(result_word))

                # Add to a list of results, to be printed at the end
                kernel_notebook_test_results[notebook_filename] = \
                    (successful_execution and test_result and
                     comparison_result)
                if not successful_execution or not test_result or \
                        not comparison_result:
                    complete_test_result = False
                    kernel_test_results = False

        # If just reading
        elif args.read:
            nb, errors = read_ipynb(notebook_filename, False, args.debug)

        # If just testing
        elif args.test:
            test_result = test(notebook_filename, args.debug)

        # If just comparing
        elif args.compare_with:
            comparison_result, error_message, error_content = \
                compare_ipynb_notebooks(
                    [notebook_filename, args.compare_with], args.debug)
            print_comparison_results([notebook_filename, args.compare_with],
                                     comparison_result, error_message,
                                     error_content, args.debug)

    # Print final results if doing validation tests for one more notebooks
    if args.execute and args.test and args.compare:
        print(BLUE, '*'.center(80, '*'), NC)
        if complete_test_result:
            print(BLUE, "** FINAL RESULT: ", GREEN, " PASSED ", NC)
        else:
            print(BLUE, "** FINAL RESULT: ", RED, " FAILED ",
                  NC)

        COLOR = GREEN
        if not kernel_test_results:
            COLOR = RED

        print(BLUE, "**", NC, "  Kernel:     ", COLOR, args.kernel_name)

        for notebook in kernel_notebook_test_results:
            COLOR = GREEN
            if not kernel_notebook_test_results[notebook]:
                COLOR = RED

            print(BLUE, "**", NC, "    Notebook: ", COLOR, notebook)
        print(BLUE, '*'.center(80, '*'), NC)

        # If assertion fails, program will exit
        print(RED)
        assert complete_test_result
        print(NC)


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
