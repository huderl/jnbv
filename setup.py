from setuptools import setup, find_packages

with open("README.md", "r") as f:
    long_description = f.read()

setup(
    name="jnbv",
    version="0.0.1",
    author="Jason Brudvik",
    author_email="jason.brudvik@maxiv.lu.se",
    description="Validation of jupyter notebooks and kernels",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
    ]
)
