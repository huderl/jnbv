# JUPYTER NOTEBOOK VALIDATION

1. [Project Overview](#project-overview)
2. [Install](#install)
2. [Development](#development)


## PROJECT OVERVIEW

This repository contains scripts used to validate ipynb notebooks and Jupyter
kernels.

The goals of this project are to be able to:
1. Execute notebooks in the terminal
2. Execute notebooks non-interactively in a CI pipeline
3. Check execution output for errors
4. Compare output of an execution to known output
5. Choose different jupyter kernels to use
6. Log results of notebook executions and tests


## INSTALL

Install into an existing virtual environment.

Create a virtual environment if you don't already have one:
```bash
pip install virtualenv
virtualenv venv
source venv/bin/activate
```

Install the code from [TestPyPi](https://test.pypi.org/project/jnbv/)
(temporarily - will use PyPi eventually):
```bash
pip install -i https://test.pypi.org/simple/ jnbv
```

Or install from gitlab:
```bash
pip install git+https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv.git
```

Install dependencies (I don't know how to have this done automatically when
install jnbv):
```bash
pip install papermill nbformat pytest
```

Use the module in a simple test in the terminal:
```bash
python
    from jnbv import validate
    validate.dummy_test()
```


## USAGE


## DEVELOPMENT

### GET THE CODE
Clone from gitlab:
```bash
git clone https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jnbv.git
cd jnbv/
```


### VIRTUAL ENVIRONMENT INSTALLATION
To use a virtual environment for devleopment work, first insall conda:
```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.9.2-Linux-x86_64.sh
sh Miniconda3-py39_4.9.2-Linux-x86_64.sh -s -p .env -b
rm Miniconda3-py39_4.9.2-Linux-x86_64.sh
```

Then install the requirements for the validation scripts into the virtual
environment:
```bash
source .env/bin/activate
conda env update --name base --file development/venv-requirements.yml
```


### RUN TESTS
Source conda environment, if not already done:
```bash
source .env/bin/activate
```

And set the python path:
```bash
export PYTHONPATH=.
```

Execute tests that are in the directory tests/:
```bash
python -m pytest
```


### BUILD
Source conda environment, if not already done:
```bash
source .env/bin/activate
```

And set the python path:
```bash
export PYTHONPATH=.
```

Build library:
```bash
python setup.py sdist bdist_wheel
```

Use module in ipython:
```bash
python
    from jnbv import validate
    validate.dummy_test()
```

![local_build_import](screenshots/local_build_import.png)

### UPLOAD

After building, first upload to [TestPyPi](https://test.pypi.org/)
(upload to PyPi later):
```bash
python3 -m twine upload --repository testpypi dist/*
    <enter username>
    <enter password>
```

![twine_upload_success](screenshots/twine_upload_success.png)


### RUN EXAMPLES
```bash
```

```bash
```
